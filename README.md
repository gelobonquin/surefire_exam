# Back-End Developer Technical Exam


## Install composer package

```bash
composer install
```

## Run database migration

```bash
php artisan migrate
```

## Fetch data from JSONPlaceholder and save to the DB

```bash
php artisan save-api-data:json

```

## Install Laravel Passport to generate Client ID and Secret

```bash
php artisan passport:client --password

```
### Example generated client secret 

``` 
What should we name the password grant client? [Laravel Password Grant Client]:
 > (Just click enter)

Password grant client created successfully.
Client ID: 1
Client secret: naHspZalTlVWNQZepg4cAGpZVz6WJQZJoTvd742u
```

### The client id and  secret will automatically save to table oauth_clients


## Create Access Token

#### Run
```bash
php artisan serve

```
#### Registered your name, email and password to http://127.0.0.1:[port]/register

#### After registration, Create POST request method to get your API Access Token

### POST REQUEST
#### end point: http://127.0.0.1:8000/oauth/token
#### body: raw (json)
#### headers: Content-Type = application/json

```bash
client_id  = Insert your generated Laravel Password Grant Client ID (example: 2)
client_secret = Insert your generated Client Secret (example: SI9EaFTdRLYeGyksqlIqjkj5JVzOYLvhMZO4G1oe)
grant_type = Default value is "password"
username = your registered email here
password = your registered password here
scope = Default value is "*"

(example)
{
	"client_id" : 1,
	"client_secret": "naHspZalTlVWNQZepg4cAGpZVz6WJQZJoTvd742u" ,
	"grant_type": "password",
	"username": "your email here",
	"password": "your password here",
	"scope": "*"
}

```
### RESPONSE  
#### It will return the token_type, expires_in, access_token and refresh_token in json format
### Example below

```
{
        "token_type" : "Bearer",
        "expires_in" : "315360000",
        "access_token" : "(long generated token)",
        "refresh_token" : "(long generated token)"
}

```

## Access API using Access Token via Postman

### GET REQUEST

### Headers
#### Authorization : token_type (white space) access_token
###### example authorization ("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJkZTg5NjQ2ZmM............")
#### Content-Type: application/x-www-form-urlencoded
#### Accept: application/json

### Resources
```

Customers
http://127.0.0.1:8000/api/customers

Todos
http://127.0.0.1:8000/api/todos

Posts
http://127.0.0.1:8000/api/posts

Photos
http://127.0.0.1:8000/api/photos

Comments
http://127.0.0.1:8000/api/comments

Albums
http://127.0.0.1:8000/api/albums

If you want to return a single data, just pass an id parameter

http://127.0.0.1:8000/api/customers/5
 

```








