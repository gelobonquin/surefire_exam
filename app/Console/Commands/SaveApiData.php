<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Exception;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use App\Post;
use App\Comment;
use App\Album;
use App\Photo;
use App\Todo;
use App\Customer;

class SaveApiData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'save-api-data:json';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get data from JSONPlaceholder and save it to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        try {
           
            $client = new Client();
            $get_post = $client->get('https://jsonplaceholder.typicode.com/posts');
            $posts = json_decode($get_post->getBody()->getContents(), false);

            $get_comments = $client->get('https://jsonplaceholder.typicode.com/comments');
            $comments = json_decode($get_comments->getBody()->getContents(), false);

            $get_albums = $client->get('https://jsonplaceholder.typicode.com/albums');
            $albums = json_decode($get_albums->getBody()->getContents(), false);

            $get_photos = $client->get('https://jsonplaceholder.typicode.com/photos');
            $photos = json_decode($get_photos->getBody()->getContents(), false);

            $get_todos = $client->get('https://jsonplaceholder.typicode.com/todos');
            $todos = json_decode($get_todos->getBody()->getContents(), false);

            $get_customers = $client->get('https://jsonplaceholder.typicode.com/users');
            $customers = json_decode($get_customers->getBody()->getContents(), false);


            DB::beginTransaction();

            // save posts
            echo $this->info("\n\nSaving Post!");
            $bar = $this->output->createProgressBar(count($posts));
            $post_data = [];
            foreach ($posts as $post) {
                $post_data[] = [
                    'id'        => $post->id,
                    'user_id'   => $post->userId,
                    'title'     => $post->title,
                    'body'      => $post->body,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }
            $bar->finish();

            if (!Post::insert($post_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;
            }

            // save comments
            echo $this->info("\n\nSaving Comments!");
            $comment_data = [];
            $bar = $this->output->createProgressBar(count($comments));
            foreach ($comments as $comment) {
                $comment_data[] = [
                    'id'        => $comment->id,
                    'post_id'   => $comment->postId,
                    'name'       => $comment->name,
                    'email'      => $comment->email,
                    'body'      => $comment->body,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }
            $bar->finish();

            if (!Comment::insert($comment_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;                
            }

            //save albums
            echo $this->info("\n\nSaving Albums!");
            $album_data = [];
            $bar = $this->output->createProgressBar(count($albums));
            foreach ($albums as $album) {
                $album_data[] = [
                    'id'        => $album->id,
                    'user_id'   => $album->userId,
                    'title'     => $album->title,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }
            $bar->finish();

            if (!Album::insert($album_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;  
            }

            //save photos
            echo $this->info("\n\nSaving Photos!");
            $photo_data = [];
            $bar = $this->output->createProgressBar(count($photos));
            foreach ($photos as $photo) {
                $photo_data[] = [
                    'id'            => $photo->id,
                    'album_id'      => $photo->albumId,
                    'title'         => $photo->title,
                    'url'           => $photo->url,
                    'thumbnail_url' => $photo->thumbnailUrl,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }
            $bar->finish();

            if (!Photo::insert($photo_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;  
            }

            // save todos
            echo $this->info("\n\nSaving Todos!");
            $todo_data = [];
            $bar = $this->output->createProgressBar(count($todos));
            foreach ($todos as $todo) {
                $todo_data[] = [
                    'id'            => $todo->id,
                    'user_id'       => $todo->userId,
                    'title'         => $todo->title,
                    'completed'     => $todo->completed,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }
            $bar->finish();

            if (!Todo::insert($todo_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;  
            }

            //save users
            echo $this->info("\n\nSaving Customers!");
            $customer_data = [];
            $bar = $this->output->createProgressBar(count($customers));
            foreach ($customers as $customer) {
                $customer_data[] = [
                    'id'            => $customer->id,
                    'name'          => $customer->name,
                    'username'      => $customer->username,
                    'email'         => $customer->email,
                    'street'        => $customer->address->street,
                    'suite'         => $customer->address->suite,
                    'city'          => $customer->address->city,
                    'zipcode'       => $customer->address->zipcode,
                    'geo_lat'       => $customer->address->geo->lat,
                    'geo_long'      => $customer->address->geo->lng,
                    'phone'         => $customer->phone,
                    'website'       => $customer->website,
                    'company_name'  => $customer->company->name,
                    'company_catch_phrase' => $customer->company->catchPhrase,
                    'company_bs'   => $customer->company->bs,
                    'created_at'    => date('Y-m-d H:i:s'),
                    'updated_at'    => date('Y-m-d H:i:s'),
                ];
                $bar->advance();
            }

            $bar->finish();

            if (!Customer::insert($customer_data)) {
                DB::rollBack();
                throw new Exception("Error Processing Request", 1);
                return;  
            }

        // Commit
        DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            echo $e->getMessage();
        }
    
    }
}
